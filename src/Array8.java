import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for(int i=0; i<arr.length;i++) {
            System.out.println("Please input arr["+ i + "]");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for(int i=0; i<arr.length;i++) {
            System.out.println(arr[i] + " ");
        }
        System.out.println();
    
        int sum = 0;
        for(int i=0; i< arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum =" + sum);
    
        double avg = (double)sum/arr.length;
        System.out.println("avg = " + avg);
        
        int minIndex = arr[0];
        for(int i=0; i < arr.length; i++) {
            if(arr[minIndex]>arr[i]) {
              minIndex = i;
            }
        }
        System.out.println("min = " + arr[minIndex] + " index = " + minIndex);
        
        int maxIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if(arr[i]>maxIndex) {
                maxIndex = arr[i];
            }
        }
        System.out.println("max = " + arr[maxIndex] + "index = " + maxIndex);
    }
}
